---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults
---
<html lang="en">
<head>
{% assign datafile = "templatedata" %}
	<title>Portfolio</title>
	<meta http-equiv="expires" content="0">
	<meta name="keywords" content="Pixolo, mobile app, mobile application development company in mumbai, freelance mobile app developers in mumbai, iOS apps, android app, freelance web development service in mumbai, web designers in mumbai, Ecommerce, E-commerce website, Software Developers in mumbai" />
	<meta name="description" content="Pixolo is one of the best Mobile App Development(iOS, Android, Windows) and Web Development company in Mumbai. It has a team of freelance Mobile App & Web Developers that can meet any specifications from a Dynamic/ECommerce Website to a Software involving Big Data Analysis.">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/pixolo_favicon.png" type="image/gif" sizes="15x15">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/slick/slick.css"/>
</head>
<body>
<nav class="heading">
	<span class="extra-space"></span>
	<a href="https://www.pixoloproductions.com/" target="_new" title="pixoloproductions"><img class="logo pix-lazy" src="images/pixolo_white.png" data-src="images/pixolo_white.png" alt="pixolo-logo"></a>
	<p class="site-link"><a class="website-link" href="https://www.pixoloproductions.com/" target="_new" title="pixoloproductions">our website</a><svg class="web-indicator" enable-background="new 0 0 492.004 492.004" version="1.1" viewBox="0 0 492 492" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
		<path d="m382.68 226.8-218.95-218.94c-5.064-5.068-11.824-7.86-19.032-7.86s-13.968 2.792-19.032 7.86l-16.124 16.12c-10.492 10.504-10.492 27.576 0 38.064l183.86 183.86-184.06 184.06c-5.064 5.068-7.86 11.824-7.86 19.028 0 7.212 2.796 13.968 7.86 19.04l16.124 16.116c5.068 5.068 11.824 7.86 19.032 7.86s13.968-2.792 19.032-7.86l219.15-219.14c5.076-5.084 7.864-11.872 7.848-19.088 0.016-7.244-2.772-14.028-7.848-19.108z"/>
</svg>
</p>
</nav>
<div class="container">
<header class="slider" id="hero-section">
	<div class="slider-title" >
		<p>Hey<span class="comma-class" >,</span></p>
  		<p>We are</p>
  		<p>Pixolo</p>
	</div>
	<div class='animated-div1' data-startx="0" data-starty="0.5" data-endx="1" data-endy="1" data-time="45"><svg class="animation-cube" width="208" height="206" viewBox="0 0 208 206" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect x="18.1309" y="113.759" width="116.876" height="113.306" transform="rotate(-35.5075 18.1309 113.759)" fill="#21FFD7" fill-opacity="0.7"/>
<path d="M42.4831 62.0092L120.099 2.85459L113.273 45.8761L18.1309 113.759L42.4831 62.0092Z" fill="#1FF3CD" fill-opacity="0.61"/>
<path d="M113.273 45.8761L120.099 2.85459L169.112 64.3439L179.082 138.112L113.273 45.8761Z" fill="#109E85" fill-opacity="0.66"/>
</svg>
</div>
<div class='animated-div2'  data-startX="0" data-startY="0" data-endX="0.5" data-endY="1" data-time="45"><svg class="animation-rect" width="58" height="58" viewBox="0 0 58 58" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M10.0569 9.36578C1.18893 13.8585 -2.35791 24.6895 2.13483 33.5575L9.36579 47.8303C13.8585 56.6983 24.6896 60.2452 33.5575 55.7524L47.8304 48.5215C56.6983 44.0287 60.2452 33.1977 55.7524 24.3297L48.5215 10.0569C44.0287 1.18891 33.1977 -2.35793 24.3297 2.13482L10.0569 9.36578ZM16.4022 11.4744C8.02683 15.7176 4.67704 25.9469 8.92019 34.3222L11.8963 40.1967C16.1395 48.572 26.3688 51.9218 34.7441 47.6786L40.6186 44.7025C48.9939 40.4593 52.3437 30.23 48.1005 21.8547L45.1244 15.9802C40.8812 7.60491 30.6519 4.25512 22.2766 8.49827L16.4022 11.4744Z" fill="#1FF3CD"/>
</svg>
</div>
<div class='animated-div3' data-startX="0" data-startY="0" data-endX="1" data-endY="1" data-time="45">
	<svg class="animation-rect2" width="59" height="59" viewBox="0 0 59 59" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M40.4184 4.67032C32.8498 -1.77502 21.4893 -0.864443 15.0439 6.70415L4.6703 18.8856C-1.77504 26.4542 -0.864459 37.8147 6.70413 44.2601L18.8856 54.6337C26.4542 61.079 37.8147 60.1685 44.2601 52.5999L54.6337 40.4184C61.079 32.8498 60.1684 21.4893 52.5998 15.0439L40.4184 4.67032ZM40.3019 12.0675C33.1538 5.98027 22.4244 6.84026 16.3371 13.9884L12.0675 19.0021C5.98025 26.1502 6.84024 36.8796 13.9884 42.9669L19.0021 47.2365C26.1502 53.3237 36.8796 52.4638 42.9668 45.3156L47.2365 40.3019C53.3237 33.1538 52.4637 22.4244 45.3156 16.3372L40.3019 12.0675Z" fill="#5E97F9"/>
</svg>

</div>
<div class='animated-div4' data-startX="0.5" data-startY="0" data-endX="1" data-endY="1" data-time="45"></div>
<div class='animated-div5' data-startX="0" data-startY="0" data-endX="1" data-endY="1" data-time="45"></div>
</header>
<main>
<section class="portfolio">
	<p class="portfolio-title">Our portfolio</p>
	<svg class="down-arrow" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129">
  <g>
    <path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"/>
  </g>
</svg>
	<p class="portfolio-headline"><span>We have got <span class="ninety-nine">99</span> problems,</span><span class="subpara-headline"> but <span class="design">design</span> ain't one</span></p>
	<div class="portfolio-wrapper">
	{% for project in site.data[datafile].projects %}
		<div class="portfolio-one">
			<p class="portfolio-type">{{project.type}}</p>
			<div class="project">
				<img class="portfolio-img pix-lazy" src="images/lazyload.png" data-src="{{project.src}}" alt="{{project.name}}">
				<a href="{{project.href}}" target="_blank" title="{{project.name}}"><div class="middle-btn">
					<button class="view-btn">View Project</button>
				</div></a>
			</div>
			<p class="portfolio-name">{{project.name}}</p>
		</div>
		{% endfor %}
	</div>
</section>
</main>
<p class="technology-title">Technologies</p>
<div class="technology-slider">
   {% for technology in site.data[datafile].technologies %}
    <div class="slider-content">
    	<img class="slider-img pix-lazy" src="images/lazyload.png" data-src="{{technology.src}}" alt="{{technology.name}}">
    	<p class="slider-name">{{technology.name}}</p>
    </div>
    {% endfor %}
  </div>
</div>
<div class="contact-details">
	<p class="portfolio-headline">Liked what you just experienced ?</p>
	<p class="portfolio-headline portfolio-call">Then give us a call at:
	
	<a href="tel:+919820840946" target="_blank" class="contact-no"><svg class="phon-logo" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 63 65" fill="none">
	<path d="M44.2746 15.669L43.0926 18.6809C48.5416 20.7368 51.1249 26.5525 49.0896 32.1816L52.1168 33.3238C55.2563 26.2094 51.7503 18.3167 44.2746 15.669ZM41.7924 21.994L40.6105 25.0059C42.3086 25.9923 43.139 27.8617 42.7325 29.783L45.7597 30.9252C47.1781 27.3109 45.5174 23.5723 41.7924 21.994ZM25.255 7.45551L4.20917 16.8041C1.89413 17.8325 0.830357 20.5432 1.84524 22.8279L18.4524 60.2145C19.4673 62.4992 22.1918 63.5272 24.5068 62.4988L45.5527 53.1502C47.8677 52.1219 48.9315 49.4112 47.9166 47.1264L31.3095 9.73986C30.2946 7.45513 27.5701 6.42717 25.255 7.45551ZM42.7848 46.9191L21.7389 56.2677L6.97703 23.0352L28.0229 13.6866L42.7848 46.9191Z" fill="#1FF3CD"/>
	</svg> +91 98208  40946</a>
	</p>
</div>
<div class="project-contact">
	<p class="plan-btn">Plan a Project</p>
		<form class="contact-form" onsubmit="return false">
		<div class="client-detail">
			<p class="details-label">your details</p>
			<div class="personal-detail-div">
				<input id="name" class="input" type="text" name="fname" placeholder="First Name">
				<input id="email" class="input" type="text" name="cemail" placeholder="E-Mail">
			</div>
		</div>
		<div>
			<p class="company-label">your company</p>
			<div class="company-detail">
				<input id="companyname" class="input" type="text" name="cname" placeholder="Company Name">
				<input id="companywebsite" class="input" type="text" name="cwebsite" placeholder="Company Website">
			</div>
			<input id="companydesc" class="input company-desc" type="text" name="cdesc" placeholder="Company Description">
		</div>
		<div class="btn-div">
			<button class="send-btn" onclick="emailSend()">Send</button>
		</div>	
		<p id="messagebox"></p>
		</form>
  </div>

<footer>
	<a href="https://www.pixoloproductions.com/" target="_new" title="pixoloproductions"><img class="footer-logo pix-lazy" src="images/pixolo_white.png" data-src="images/pixolo_white.png" alt="pixolo-logo"></a>
</footer>
<div class="social-links">
	<div class="links"><a href="https://in.pinterest.com/pixoloproduction/" target="_new" title="pixolo-pinterest">Pinterest</a></div>
	<div class="links"><a href="https://www.instagram.com/productionspixolo/" target="_new" title="pixolo-instagram">Instagram</a></div>
	<div class="links"><a href="https://www.facebook.com/PixoloProductions/" target="_new" title="pixolo-facebook">Facebook</a></div>
</div>
<!--clip path-->
	<script type="text/javascript" src="js/jquery.min.js"></script>
	  <script type="text/javascript" src="js/slick.min.js"></script>
	  <script type="text/javascript">
	//	SLICK SLIDER
	    $(document).ready(function(){
	      $('.technology-slider').slick({
	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 1,
			autoplay: true,
	    	autoplaySpeed: 1500,
			cssEase: 'linear',
	  prevArrow: '<div ><svg class="prev-arrow" width="199.4px" height="199.4px" enable-background="new 0 0 199.404 199.404" version="1.1" viewBox="0 0 199.404 199.404" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><polygon points="135.41 0 35.709 99.702 135.41 199.4 163.7 171.12 92.277 99.702 163.7 28.285"/></svg></div>',
	  nextArrow: '<div ><svg class="next-arrow" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="306px" height="306px" viewBox="0 0 306 306" style="enable-background:new 0 0 306 306;" xml:space="preserve"><g><g id="chevron-right"><polygon points="94.35,0 58.65,35.7 175.95,153 58.65,270.3 94.35,306 247.35,153"/></g></g></svg></div>',
	responsive: [
	    {
	      breakpoint: 1025,
	      settings: {
			slidesToShow: 2,
	        slidesToScroll: 1,
	        infinite: true,
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]		  
	});
		})
	
		function horizontalscroll(){
		var y=$(".horz-pix-lazy .pix-lazy");$(y).each(function(y){isElementInViewport(this)&&""!=$(this).data("src")&&($(this).attr("src",$(this).attr("data-src")),$(this).removeAttr("data-src"),$(this).removeClass("pix-lazy"))})}

//	LAZY LOADING
function isElementInViewport(t){var i=t.getBoundingClientRect();return i.top>=0&&i.left<=$(window).width()&&i.bottom<=$(window).height()&&i.right>=0};
function lazyloadingonscroll(){var t=$(".pix-lazy");$(t).each(function(t){isElementInViewport(this)&&""!=$(this).data("src")&&($(this).attr("src",$(this).attr("data-src")),$(this).removeAttr("data-src"),$(this).removeClass("pix-lazy"))})}

setInterval(function(){
	lazyloadingonscroll();
}, 1000);

$(window).on("DOMContentLoaded load resize scroll",lazyloadingonscroll),$(".horz-pix-lazy").scroll(horizontalscroll());
	  </script>

  <script>
//	SLIDER DIVS MOVING ANIMATION
	$(document).ready(function(){
		animateDiv('.animated-div1');
		animateDiv('.animated-div2');
		animateDiv('.animated-div3');
		animateDiv('.animated-div4');
		animateDiv('.animated-div5');	
	});

var makeNewPosition = function(cls, sx, sy, ex, ey){
	
    var maxw = $('#hero-section').width()*ex;
    var minw = $('#hero-section').width()*sx;
    var maxh = $('#hero-section').height()*ey;
    var minh = $('#hero-section').height()*sy;
	
	
	
    
    var nh = Math.floor(Math.random()*(maxh-minh+1)+minh);
    var nw = Math.floor(Math.random()*(maxw-minw+1)+minw);
	
<!--	console.log(cls, [nh,nw])-->
    
    return [nh,nw];    
    
}

function animateDiv(myclass){
<!--console.log($(myclass).data('startx'));-->
if($(myclass).data('startx')!=undefined){
	//this will check if data-x attribute exists
	var newq = makeNewPosition(myclass, $(myclass).data('startx'), $(myclass).data('starty'),$(myclass).data('endx'), $(myclass).data('endy'));
    $(myclass).animate({ top: newq[0], left: newq[1] }, ($(myclass).data('time')*1000),   function(){
      animateDiv(myclass);        
    });
}
  
  
  
    
};
</script>

<script>
//	LOGO ANIMATION ON SCROLL
	var lastScrollTop = 0;
$(window).scroll(function(event){
   var st = $(this).scrollTop();
   if (st > lastScrollTop){
       $('.logo').css({"clip-path": "polygon(30% 0, 50% 0, 50% 100%, 30% 100%)", "transform": "translateX(50px)"});
   } else {
	   $('.logo').css({"clip-path": "polygon(0 0, 100% 0, 100% 100%, 0% 100%)", "transform": "translateX(0px)"});
   }
   lastScrollTop = st;
});
</script>
<script>
//EMAIL SENDING
function emailSend() {
	var email = document.getElementById("email").value;
	var name = document.getElementById("name").value;
	var companyname = document.getElementById("companyname").value;
	var companywebsite = document.getElementById("companywebsite").value;
	var companydesc = document.getElementById("companydesc").value;
	//VALIDATION FORM //
	var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
	if (email == "" || name == "" || companyname == "" || companywebsite == "" || companydesc == "" ) {
		document.getElementById("messagebox").innerHTML = "Please enter all values";
		setInterval(function() {document.getElementById("messagebox").innerHTML = "";},5000);
	} else if ( !email.match(reEmail)) {
		document.getElementById("messagebox").innerHTML = "Please enter valid values";
	} else {
		var xhttp = new XMLHttpRequest();
		var requestData = "email=" + email + "&name=" + name + "&companyname=" + companyname + "&companywebsite=" + companywebsite + "&companydesc=" + companydesc;
		xhttp.open("POST", "https://www.pixoloproductions.com/email/portfoliomail.php", true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send(requestData);
		xhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				var responseValue = this.responseText;
				if (responseValue == 1) {
					document.getElementById("messagebox").innerHTML = "Your Response Sent Successfully";
					document.getElementById("messagebox").style.color = "#6bbd6e";
					document.getElementById("email").value = document.getElementById("email").defaultValue;
					document.getElementById("name").value = document.getElementById("name").defaultValue;
					document.getElementById("companyname").value = document.getElementById("companyname").defaultValue;
					document.getElementById("companywebsite").value = document.getElementById("companywebsite").defaultValue;
					document.getElementById("companydesc").value = document.getElementById("companydesc").defaultValue;
				} else {
					document.getElementById("messagebox").innerHTML = "Your Response Not Sent";
				}
			}
		};
	}
}

</script>
</body>

</html>

