
window.onload = function () {


	//MAKE DUPLICATES
	Array.prototype.forEach.call(document.getElementsByClassName('frame'), function (element, index) {

		for (var cl = 0; cl < 10; cl++) {
			var clone = element.cloneNode(true);
			clone.classList.remove('frame');
			clone.classList.add('frame-clone');
			document.getElementById("frame-section").appendChild(clone);
		};
	});

	var secHeight = document.getElementById('frame-section').offsetHeight / 2;
	window.onscroll = myScroll;

	function myScroll() {
		//UNDERLINE OF ABOUT//
		Array.prototype.forEach.call(document.getElementsByClassName('about-title'), function (element, index) {
			var topAboutValue = document.getElementsByClassName('about-title')[index].getBoundingClientRect().top;
			if (topAboutValue < 600) {
				document.getElementsByClassName('about-title')[index].classList.add('add');
			}
		});


		//MOBILE IMAGE ANIMATION//
		var topValue = document.getElementById('frame-section').getBoundingClientRect().top;
		if (topValue < secHeight) {
			if (topValue > 0) {
				var percentValue = ((secHeight - topValue) * 100) / secHeight;
				Array.prototype.forEach.call(document.getElementsByClassName('frame'), function (element, index) {
					var finalValue = index * 7;
					var bottomValue = (percentValue * finalValue) / 100;
					element.style.bottom = bottomValue + '%';
					Array.prototype.forEach.call(document.getElementsByClassName("frame" + (index + 1)), function (element, index) {
						if (index != 0) {
							bottomValue += 0.05;
							element.style.bottom = bottomValue + '%';
						};
					});
				});
			}
		};
	}
};

//SCROLLING MOBILE SLIDER//






//var scrollleft;
//var x = document.getElementsByClassName("mobile-slider")[0];
//var sliderWidth = x.scrollWidth - document.body.offsetWidth;
//var value = 10;
//var interval;
//
//function scrolling() {
//	interval = setInterval(function () {
//		if (x.scrollLeft == 0) {
//			scrollleft = true;
//		} else if (x.scrollLeft >= (sliderWidth - 1)) {
//			scrollleft = false;
//		}
//		if (scrollleft) {
//			x.scrollLeft += value;
//		} else {
//			x.scrollLeft -= value;
//		}
//	}, 100);
//
//}
//scrolling();
//
//x.addEventListener('mouseout', function () {
//	scrolling();
//});
//
//x.addEventListener('mouseover', function () {
//	clearInterval(interval);
//});


//EMAIL SENDING
function emailSend() {

	var email = document.getElementById("email_text").value;
	var name = document.getElementById("name_text").value;
	var contact = document.getElementById("contact_text").value;
	var message = document.getElementById("msg_text").value;

	//VALIDATION FORM //
	var reEmail = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;

	if (email == "" || name == "" || contact == "" || message == "") {

		document.getElementById("messagebox").innerHTML = "** Please enter all values **";

	} else if (contact.length > 10 || contact.length < 10 || !email.match(reEmail)) {

		document.getElementById("messagebox").innerHTML = "** Please enter valid values **";

	} else {

		var xhttp = new XMLHttpRequest();
		var requestData = "email=" + email + "&name=" + name + "&contact=" + contact + "&message=" + message;
		xhttp.open("POST", "https://www.pixoloproductions.com/email/sendmail.php", true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send(requestData);
		xhttp.onreadystatechange = function () {

			if (this.readyState == 4 && this.status == 200) {
				var responseValue = this.responseText;
				if (responseValue == 1) {

					document.getElementById("messagebox").innerHTML = "** Your Response Sent Successfully **";
					document.getElementById("email_text").value = document.getElementById("email_text").defaultValue;
					document.getElementById("name_text").value = document.getElementById("name_text").defaultValue;
					document.getElementById("contact_text").value = document.getElementById("contact_text").defaultValue;
					document.getElementById("msg_text").value = document.getElementById("msg_text").defaultValue;

				} else {

					document.getElementById("messagebox").innerHTML = "** Your Response Not Sent **";

				}

			}
		};
	}
}

//FEATURES ANIMATION
var count = (document.getElementsByClassName('feature-row').length) - 1;
var windowSize = window.matchMedia("(max-width: 992px)");
var k;
for (k = 0; k <= count; k++) {
	if (!windowSize.matches) {
		if (k % 2 != 0) {
			document.getElementsByClassName('feature-row')[k].children[0].classList.add('fadeInRight');
			document.getElementsByClassName('feature-row')[k].children[1].classList.add('fadeInLeft');
		} else {
			document.getElementsByClassName('feature-row')[k].children[0].classList.add('fadeInLeft');
			document.getElementsByClassName('feature-row')[k].children[1].classList.add('fadeInRight');
		}
	} else {
		document.getElementsByClassName('feature-row')[k].children[0].classList.add('fadeInRight');
		document.getElementsByClassName('feature-row')[k].children[1].classList.add('fadeInLeft');
	}
}

//SMOOTH SCROLLING
document.querySelectorAll('a[href^="#"]').forEach(a => {
	a.addEventListener('click', function (e) {
		e.preventDefault();
		var href = this.getAttribute("href");
		var elem = document.querySelector(href) || document.querySelector("a[name=" + href.substring(1, href.length) + "]");
		window.scroll({
			top: elem.offsetTop,
			left: 0,
			behavior: 'smooth'
		});
	});
});
